{ mkDerivation, base, relude, containers, lucid, stdenv, text }:
mkDerivation {
  pname = "hl-i809";
  version = "0.1.5.0";
  src = /home/gander/PROJ/haskell/hl-i809;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base containers relude lucid text ];
  license = stdenv.lib.licenses.bsd3;
}
