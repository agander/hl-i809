{
  nixpkgs ? import <nixpkgs> {}, compiler ? "ghc884"
}:

nixpkgs.pkgs.haskell.packages.${compiler}.callPackage ./hl-i809.nix { }

