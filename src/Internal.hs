{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Internal
(
  test_data
  , thHead
  , thRow
  , isValue
  , lol_to_table
  , lol_to_rows
)
where

--import Relude
--import Relude.Container

--import Data.IntMap (IntMap)
--import qualified Data.IntMap.Strict as IMS
--import Data.Map.Strict (Map)
--import qualified Data.Map.Strict as MS
--import Data.Char (isDigit)
--import Data.List
--import Safe
--import qualified System.Console.CmdArgs.Explicit as C

--import System.Exit
--import System.FilePath
--import System.Process
--import Text.Printf

--import Data.Text (Text)
import qualified Data.Text.Lazy as TL
import qualified Data.Text as TS

--import GHC.IO.Encoding
--import Control.Monad

--import Data.Maybe (fromMaybe)
import Data.String (IsString)
import qualified Lucid as L
--import System.Environment (getEnvironment)
import Prelude hiding (lookup)

--

-- | Let's go!

-- | Functions |--
test_data :: Data.String.IsString a => [[a]]
test_data =
  [ ["Date", "Payee", "Account1", "Amount", "Account2", "Amount"],
    ["2008-01-01", "income", "assets:bank:checking", "$", "1", "income:salary", "$", "-1"],
    ["2008-06-01", "gift", "assets:bank:checking", "$", "1", "income:gifts", "$", "-1"],
    ["2008-06-02", "save", "assets:bank:saving", "$", "1", "assets:bank:checking", "$", "-1"],
    ["2008-06-03", "eat & shop", "expenses:food", "$", "1", "expenses:supplies", "$", "1"],
    ["2008-12-31", "pay off", "liabilities:debts", "$", "1", "assets:bank:checking", "$", "-1"]
  ]

thRow :: [String] -> L.Html ()
thRow = L.tr_ . mconcat . map (L.td_ . L.toHtml)

thHead :: [String] -> L.Html ()
thHead = L.tr_ . mconcat . map (L.th_ . L.toHtml)

isValue "-" = True
isValue ('-' : _) = False
isValue _ = True

-- | Render the HTML table rows for a BalanceReport.
-- Returns the heading row, 0 or more body rows, and the totals row if enabled.
lol_to_rows :: [[String]] -> (L.Html (), [L.Html ()])
lol_to_rows [] = mempty  -- shouldn't happen
lol_to_rows (headings:rest) = 
  let hr = thHead headings
      drs = mapM_ thRow rest
  in (hr, [drs])

{-
lol_to_rows (headings:rest) =
  let
    defstyle = L.style_ ""
    amts = rest
  in
    L.tr_ $ mconcat $
          L.td_ [L.class_ "account"]              (L.toHtml headings)
       : [L.td_ [L.class_ "", defstyle]           (L.toHtml a) | a <- amts]
      -- ++ [L.td_ [L.class_ "rowtotal", defstyle]   (L.toHtml a) | a <- tot]
      -- ++ [L.td_ [L.class_ "rowaverage", defstyle] (L.toHtml a) | a <- avg]
-}

-- | Render a multi-column balance report as HTML.
lol_to_table :: [[String]] -> L.Html()
lol_to_table lists =
  let (headingsrow, bodyrows) = lol_to_rows lists
  in do
    {-
    -}
    L.style_ (TS.unlines [""
      ,"td { padding:0 0.5em; }"
      ,"td:nth-child(1) { white-space:nowrap; }"
      ,"tr:nth-child(even) td { background-color:#eee; }"
      ])
    L.link_ [L.rel_ "stylesheet", L.href_ "hledger.css"]
    L.table_ $ mconcat $ [headingsrow] ++ bodyrows

{-
    table_ $ mconcat $
         titlerows
      ++ [blankrow]
-}
