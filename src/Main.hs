{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Main where

import qualified Internal as I
--import Relude
--import Relude.Container

--import Data.IntMap (IntMap)
--import qualified Data.IntMap.Strict as IMS
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as MS
--import Data.Char (isDigit)
--import Data.List
--import Safe
--import qualified System.Console.CmdArgs.Explicit as C

--import System.Exit
--import System.FilePath
--import System.Process
--import Text.Printf

--import Data.Text ()
import qualified Data.Text.Lazy as T

--import GHC.IO.Encoding
--import Control.Monad

import Data.Maybe (fromMaybe)
import Data.String (IsString)
import qualified Lucid as L
import System.Environment (getEnvironment)
import Prelude hiding (lookup)

--

-- | Let's go!
main :: IO ()
main = do
  -- _ <- withProgName "" print
  vars <- getEnvironment
  let myHashMap = MS.fromList [("a", "xxx"), ("b", "yyy")]
  putStr "48:myHashMap:toAscList "
  print $ MS.toAscList myHashMap
  putStr "50:myHashMap:unzip"
  print $ unzip [("a", "xxx"), ("b", "yyy")]

  let env_hashmap = MS.fromList vars
  --print $ MS.toAscList env_hashmap
  putStr "55:lookup:HISTFILE: "
  print $ fromMaybe "" $ MS.lookup "HISTFILE" env_hashmap
  putStr "57:lookupMax:env_hashmap: "
  print $ MS.lookupMax env_hashmap
  putStrLn "!!! DEBUUG:57:thHead:head test_data: "
  print $ I.thHead $ head I.test_data
  putStrLn "!!! DEBUUG:59:thRow:tail test_data: "
  mapM_ (print . I.thRow) (tail I.test_data)
  putStr "63:init:test_data: "
  print $ init I.test_data
  putStr "65:head:test_data: "
  print $ head I.test_data
  --putStr "65:last:test_data: "
  --print $ last test_data
  --let page2 = L.html_ (do L.head_ ( do L.title_ "Introduction page."
  {-
  let page1 =
              L.html_ (do L.head_ (do L.title_ "Introduction page."
                                    L.with L.link_ [L.rel_ "stylesheet" ,L.type_ "text/css" ,L.href_ "screen.css"])
                          L.body_ (do L.with L.div_ [L.id_ "header"] "Syntax"
                                    L.p_ "This is an example of Lucid syntax."
                                    L.ul_ (mapM_ (L.li_ . L.toHtml . show) [1,2,3])))
  -}
  --let page = I.lol_to_table I.test_data
  print $ I.lol_to_table I.test_data
  print $ (++"\n") $ T.unpack $ L.renderText $ I.lol_to_table I.test_data
  -- "html" -> (++"\n") $ TL.unpack $ L.renderText $ compoundBalanceReportAsHtml ropts cbr
  return ()

-- | Functions |--

